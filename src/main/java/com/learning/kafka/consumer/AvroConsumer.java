package com.learning.kafka.consumer;

import com.learning.kafka.KeyData;
import com.learning.kafka.ValueData;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import org.apache.kafka.clients.consumer.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

public class AvroConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(AvroConsumer.class);

    public static void main(String[] args) {

        Properties properties = new Properties();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class);
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class);
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, "avro_output_group");
        properties.put(KafkaAvroDeserializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");

        Consumer<KeyData, ValueData> consumer = new KafkaConsumer<>(properties);
        consumer.subscribe(Collections.singletonList("avro_output"));

        while (true) {
            ConsumerRecords<KeyData, ValueData> records = consumer.poll(Duration.ofMillis(100));
            for (ConsumerRecord<KeyData, ValueData> record : records) {
                LOGGER.info("-----Key: {}, Value: {}", record.key(), record.value());
            }
        }
    }
}
