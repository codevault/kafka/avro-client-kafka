package com.learning.kafka.producer;

import com.learning.kafka.KeyData;
import com.learning.kafka.ValueData;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.function.Supplier;

public class SingleMessageAvroProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(SingleMessageAvroProducer.class);
    private static final Supplier<KeyData> KEY_DATA = () -> new KeyData(1L, "KeyData");
    private static final Supplier<ValueData> VALUE_DATA = () -> new ValueData(1L, "ValueData");

    public static void main(String[] args) {

        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);
        properties.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");

        Producer<KeyData, ValueData> producer = new KafkaProducer<>(properties);

        KeyData key = KEY_DATA.get();
        ValueData value = VALUE_DATA.get();
        LOGGER.info("-----Key: {}, Value: {}", key, value);
        ProducerRecord<KeyData, ValueData> record = new ProducerRecord<>("avro_input", key, value);
        producer.send(record);

        producer.close();
    }
}
