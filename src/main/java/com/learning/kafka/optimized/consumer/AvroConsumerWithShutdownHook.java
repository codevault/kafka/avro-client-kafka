package com.learning.kafka.optimized.consumer;

import com.learning.kafka.KeyData;
import com.learning.kafka.ValueData;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.errors.WakeupException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;

public class AvroConsumerWithShutdownHook {

    private static final Logger LOGGER = LoggerFactory.getLogger(AvroConsumerWithShutdownHook.class);

    public static void main(String[] args) {

        Properties properties = new Properties();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class);
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class);
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, "avro_output_group");
        properties.put(KafkaAvroDeserializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");

        Consumer<KeyData, ValueData> consumer = new KafkaConsumer<>(properties);
        consumer.subscribe(Collections.singletonList("avro_output"));

        AtomicBoolean shutdownRequested = new AtomicBoolean(false);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            shutdownRequested.set(true);
            consumer.wakeup();
        }));

        try {
            while (!shutdownRequested.get()) {
                ConsumerRecords<KeyData, ValueData> records = consumer.poll(Duration.ofMillis(100));
                for (ConsumerRecord<KeyData, ValueData> record : records) {
                    LOGGER.info("-----Key: {}, Value: {}", record.key(), record.value());
                }
            }
        } catch (WakeupException e) {
            LOGGER.warn("WakeupException: {}", e.getMessage());
        } finally {
            consumer.close();
        }
    }
}
