package com.learning.kafka.optimized.producer;

import com.learning.kafka.KeyData;
import com.learning.kafka.ValueData;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import org.apache.kafka.clients.producer.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.function.Function;

public class BulkMessageAsynchronousAvroProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(BulkMessageAsynchronousAvroProducer.class);
    private static final Function<Integer, KeyData> KEY_DATA = i -> new KeyData((long) i, "KeyData_" + i);
    private static final Function<Integer, ValueData> VALUE_DATA = i -> new ValueData((long) i, "ValueData_" + i);

    public static void main(String[] args) {

        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);
        properties.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");

        Producer<KeyData, ValueData> producer = new KafkaProducer<>(properties);

        for (int i = 0; i < 1000000; i++) {
            KeyData key = KEY_DATA.apply(i);
            ValueData value = VALUE_DATA.apply(i);
            LOGGER.info("-----Key: {}, Value: {}", key, value);
            ProducerRecord<KeyData, ValueData> record = new ProducerRecord<>("avro_input", key, value);
            producer.send(record, new Callback() {
                @Override
                public void onCompletion(RecordMetadata metadata, Exception exception) {
                    if (exception == null) {
                        LOGGER.info("Message sent successfully to topic: {}, partition: {}, offset: {}",
                                metadata.topic(), metadata.partition(), metadata.offset());
                    } else {
                        LOGGER.error("Error sending message: " + exception.getMessage());
                    }
                }
            });
        }

        producer.close();
    }
}
